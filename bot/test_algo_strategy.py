from algo_strategy import AlgoStrategy
import unittest
from unittest.mock import call
from unittest.mock import Mock
from unittest.mock import patch


class TestAlgoStrategy(unittest.TestCase):

    def setUp(self):
        self.game_state = Mock()
        with patch("algo_strategy.gamelib.debug_write") as debug_write_mocked:
            self.algo_strategy = AlgoStrategy()
            config = {
                "unitInformation": [
                    {"shorthand": "WALL"}, {"shorthand": "SUPPORT"}, {"shorthand": "TURRET"}, {"shorthand": "SCOUT"},
                    {"shorthand": "DEMOLISHER"}, {"shorthand": "INTERCEPTOR"}
                ]
            }
            self.algo_strategy.on_game_start(config)

    def test_execute_strategy_less_than_five_turns(self):
        with patch("algo_strategy.AlgoStrategy.build_static_defense") as build_static_defense_mocked:
            with patch("algo_strategy.AlgoStrategy.build_reactive_defense") as build_reactive_defense_mocked:
                for turn_number in range(0, 5):
                    self.game_state.turn_number = turn_number

                    self.algo_strategy.execute_strategy(self.game_state)

                self.assertEqual(build_static_defense_mocked.call_count, 5)
                self.assertEqual(build_reactive_defense_mocked.call_count, 5)
                self.assertEqual(self.game_state.attempt_spawn.call_count, 0)

    def test_execute_strategy_five_and_more_turns(self):
        with patch("algo_strategy.AlgoStrategy.build_static_defense") as build_static_defense_mocked:
            with patch("algo_strategy.AlgoStrategy.build_reactive_defense") as build_reactive_defense_mocked:
                with patch("algo_strategy.AlgoStrategy.support_tactic") as support_tactic_mocked:
                    with patch("algo_strategy.AlgoStrategy.least_damage_spawn_location") as least_damage_location_mocked:
                        with patch("algo_strategy.AlgoStrategy.get_spawn_group_of_location") as get_spawn_group_of_location_mocked:
                            with patch("algo_strategy.AlgoStrategy.equip_spawn_group") as equip_spawn_group_mocked:
                                with patch("algo_strategy.AlgoStrategy.get_highest_coverage_spawn_groups") as get_highest_coverage_spawn_groups_mocked:
                                    with patch("algo_strategy.AlgoStrategy.get_locations_for_spawn_groups") as get_locations_for_spawn_groups_mocked:
                                        support_tactic_mocked.side_effect = [None, None, [6, 7], [6, 7], None, None]
                                        least_damage_location_mocked.return_value = [6, 7]
                                        get_spawn_group_of_location_mocked.return_value = 0
                                        get_highest_coverage_spawn_groups_mocked.return_value = [0]
                                        get_locations_for_spawn_groups_mocked.return_value = [[6, 7], [7, 6]]

                                        self.assertEqual(self.algo_strategy.demolisher_assault_mode, False)

                                        self.game_state.turn_number = 5
                                        self.algo_strategy.execute_strategy(self.game_state)
                                        self.assertEqual(self.algo_strategy.demolisher_assault_mode, False)
                                        self.assertEqual(support_tactic_mocked.call_count, 1)
                                        self.assertEqual(least_damage_location_mocked.call_count, 1)
                                        self.assertEqual(get_spawn_group_of_location_mocked.call_count, 1)
                                        get_spawn_group_of_location_mocked.assert_called_with([6, 7])
                                        self.assertEqual(equip_spawn_group_mocked.call_count, 1)
                                        equip_spawn_group_mocked.assert_called_with(0, self.game_state)
                                        self.assertEqual(self.game_state.attempt_spawn.call_count, 1)
                                        self.game_state.attempt_spawn.assert_called_with("SCOUT", [6, 7], 1000)

                                        self.algo_strategy.scored_last_turn = False
                                        self.game_state.turn_number = 6
                                        self.algo_strategy.execute_strategy(self.game_state)
                                        self.assertEqual(support_tactic_mocked.call_count, 1)
                                        self.assertEqual(self.algo_strategy.demolisher_assault_mode, True)
                                        self.assertEqual(self.game_state.attempt_spawn.call_count, 1)

                                        self.game_state.turn_number = 7
                                        self.algo_strategy.execute_strategy(self.game_state)
                                        self.assertEqual(support_tactic_mocked.call_count, 1)
                                        self.assertEqual(self.algo_strategy.demolisher_assault_mode, True)
                                        self.assertEqual(self.game_state.attempt_spawn.call_count, 1)
                                        self.game_state.turn_number = 8
                                        self.algo_strategy.execute_strategy(self.game_state)
                                        self.assertEqual(support_tactic_mocked.call_count, 2)
                                        support_tactic_mocked.assert_called_with(self.game_state)
                                        self.assertEqual(self.algo_strategy.demolisher_assault_mode, True)
                                        self.assertEqual(self.game_state.attempt_spawn.call_count, 1)
                                        self.game_state.turn_number = 9
                                        self.algo_strategy.execute_strategy(self.game_state)
                                        self.assertEqual(support_tactic_mocked.call_count, 3)
                                        self.assertEqual(self.game_state.attempt_spawn.call_count, 2)
                                        self.game_state.attempt_spawn.assert_called_with("DEMOLISHER", [6, 7], 1000)
                                        self.assertEqual(self.algo_strategy.demolisher_assault_mode, False)

                                        self.algo_strategy.scored_last_turn = True
                                        self.game_state.turn_number = 10
                                        self.algo_strategy.execute_strategy(self.game_state)
                                        self.assertEqual(self.algo_strategy.demolisher_assault_mode, False)
                                        self.assertEqual(support_tactic_mocked.call_count, 4)
                                        self.assertEqual(self.game_state.attempt_spawn.call_count, 3)
                                        self.game_state.attempt_spawn.assert_called_with("SCOUT", [6, 7], 1000)

                                        self.algo_strategy.scored_last_turn = False
                                        self.algo_strategy.execute_strategy(self.game_state)
                                        self.assertEqual(self.algo_strategy.turns_since_last_assault, 1)
                                        self.assertEqual(self.algo_strategy.demolisher_assault_mode, True)
                                        self.algo_strategy.execute_strategy(self.game_state)
                                        self.assertEqual(self.algo_strategy.turns_since_last_assault, 2)
                                        self.algo_strategy.execute_strategy(self.game_state)
                                        self.assertEqual(self.algo_strategy.turns_since_last_assault, 3)
                                        self.algo_strategy.execute_strategy(self.game_state)
                                        self.assertEqual(get_highest_coverage_spawn_groups_mocked.call_count, 1)
                                        get_highest_coverage_spawn_groups_mocked.assert_called_with(self.game_state)
                                        self.assertEqual(get_locations_for_spawn_groups_mocked.call_count, 1)
                                        get_locations_for_spawn_groups_mocked.assert_called_with([0])
                                        least_damage_location_mocked.assert_called_with(self.game_state, [[6, 7], [7, 6]])
                                        get_spawn_group_of_location_mocked.assert_called_with([6, 7])
                                        equip_spawn_group_mocked.assert_called_with(0, self.game_state)
                                        self.assertEqual(self.game_state.attempt_spawn.call_count, 4)
                                        self.game_state.attempt_spawn.assert_called_with("DEMOLISHER", [6, 7], 1000)
                                        self.assertEqual(self.algo_strategy.demolisher_assault_mode, False)
                                        self.assertEqual(self.algo_strategy.turns_since_last_assault, 0)

    def test_build_static_defense(self):
        self.algo_strategy.build_static_defense(self.game_state)

        turret_locations = [[0, 13], [4, 12], [23, 12], [27, 13], [8, 11], [19, 11], [13, 11], [14, 11]]
        wall_locations = [[4, 13], [23, 13], [8, 12], [19, 12], [13, 12], [14, 12]]
        self.game_state.attempt_spawn.assert_has_calls([call("TURRET", turret_locations), call("WALL", wall_locations)], any_order=False)
        self.game_state.attempt_upgrade.assert_has_calls([call(wall_locations), call(turret_locations)], any_order=False)
        
    def test_build_reactive_defense(self):
        with patch("algo_strategy.AlgoStrategy.improve_defense_near_spawn_location") as improve_defense_near_spawn_location_mocked:
            self.game_state.attempt_spawn.return_value = 1
            input_locations = []
            input_locations.append(self.algo_strategy.non_reserved_edge_locations[0])
            input_locations.append([2, 11])
            input_locations.append([6, 7])
            self.algo_strategy.scored_on_locations = input_locations

            self.algo_strategy.build_reactive_defense(self.game_state)

            self.assertEqual(self.game_state.attempt_spawn.call_count, 3)
            self.game_state.attempt_spawn.assert_has_calls([call("TURRET", self.algo_strategy.non_reserved_edge_locations[0]), call("TURRET", [2, 11]), call("WALL", [2, 12])], any_order=False)
            self.game_state.attempt_upgrade.assert_called_with([2, 12])
            self.assertEqual(improve_defense_near_spawn_location_mocked.call_count, 1)
            improve_defense_near_spawn_location_mocked.assert_called_with(self.game_state, [6, 7])

    def test_improve_defense_near_spawn_location(self):
        with patch("algo_strategy.AlgoStrategy.spawn_turret_if_no_blockage_of_spawn_group") as spawn_turret_if_no_blockage_of_spawn_group_mocked:
            with patch("algo_strategy.AlgoStrategy.upgrade_turret_of_blockage_triggering_location_combinations") as upgrade_turret_of_blockage_triggering_location_combinations_mocked:
                self.game_state.attempt_spawn.return_value = 1
                spawn_turret_if_no_blockage_of_spawn_group_mocked.side_effect = [True, True, False]
                self.game_state.game_map.in_arena_bounds.side_effect = [True, False]
                input_locations = []
                input_locations.append([4, 9])
                input_locations.append([12, 1])
                input_locations.append([12, 1])
                call_locations = []
                call_locations.append([4, 11])
                call_locations.append([14, 1])
                call_locations.append([10, 1])
                expected_calls = []
                for call_location in call_locations:
                    expected_calls.append(call(self.game_state, call_location))

                for input_location in input_locations:
                    self.algo_strategy.improve_defense_near_spawn_location(self.game_state, input_location)

                self.assertEqual(spawn_turret_if_no_blockage_of_spawn_group_mocked.call_count, 3)
                spawn_turret_if_no_blockage_of_spawn_group_mocked.assert_has_calls(expected_calls, any_order=False)
                self.assertEqual(self.game_state.attempt_spawn.call_count, 2)
                self.game_state.attempt_spawn.assert_has_calls([call("WALL", [4, 12]), call("WALL", [14, 3])])
                upgrade_turret_of_blockage_triggering_location_combinations_mocked.assert_called_with(self.game_state, [10, 1])
                self.game_state.attempt_upgrade.assert_has_calls([call([4, 12]), call([14, 3])])


    def test_spawn_turret_if_no_blockage_of_spawn_group_blockage_case(self):
        input_locations = [[8, 7], [10, 5], [13, 1], [13, 2]]
        contains_stationary_unit_return_values = []
        contains_stationary_unit_return_values += [True, True, False]
        contains_stationary_unit_return_values += [True, False]
        contains_stationary_unit_return_values += [True, False]
        contains_stationary_unit_return_values += [False, True]
        self.game_state.contains_stationary_unit.side_effect = contains_stationary_unit_return_values
        expected_call_locations = [[6, 9], [7, 8], [8, 7], [10, 5], [11, 4], [14, 2], [13, 1], [13, 2], [14, 1]]
        expected_calls = []
        for expected_call_location in expected_call_locations:
            expected_calls.append(call(expected_call_location))

        for input_location in input_locations:
            self.assertEqual(self.algo_strategy.spawn_turret_if_no_blockage_of_spawn_group(self.game_state, input_location), False)
        self.assertEqual(self.game_state.contains_stationary_unit.call_count, len(contains_stationary_unit_return_values))
        self.game_state.contains_stationary_unit.assert_has_calls(expected_calls, any_order = False)
        self.assertEqual(self.game_state.attempt_spawn.call_count, 0)

    def test_spawn_turret_if_no_blockage_of_spawn_group_non_blockage_case(self):
        input_locations = [[8, 7], [10, 5], [13, 1], [13, 2]]
        contains_stationary_unit_return_values = []
        contains_stationary_unit_return_values += [True, False, False]
        contains_stationary_unit_return_values += [False, False]
        contains_stationary_unit_return_values += [False, False, False, False]
        contains_stationary_unit_return_values += [False, False, False, False]
        self.game_state.contains_stationary_unit.side_effect = contains_stationary_unit_return_values
        expected_call_locations = [[6, 9], [7, 8], [8, 7], [10, 5], [11, 4], [14, 2], [13, 1], [13, 1], [14, 1], [13, 2], [14, 1], [13, 2], [14, 2]]
        expected_calls = []
        for expected_call_location in expected_call_locations:
            expected_calls.append(call(expected_call_location))
        self.game_state.attempt_spawn.side_effect = [0, 0, 1, 1]

        i = 0
        for input_location in input_locations:
            if i >= 2:
                self.assertEqual(self.algo_strategy.spawn_turret_if_no_blockage_of_spawn_group(self.game_state, input_location), True)
            else:
                self.assertEqual(self.algo_strategy.spawn_turret_if_no_blockage_of_spawn_group(self.game_state, input_location), False)
            i += 1
        self.assertEqual(self.game_state.contains_stationary_unit.call_count, len(contains_stationary_unit_return_values))
        self.game_state.contains_stationary_unit.assert_has_calls(expected_calls, any_order = False)
        self.assertEqual(self.game_state.attempt_spawn.call_count, 4)

    def test_upgrade_turret_of_blockage_triggering_location_combinations(self):
       self.game_state.contains_stationary_unit.side_effect = [False, False, True]
       self.algo_strategy.upgrade_turret_of_blockage_triggering_location_combinations(self.game_state, [7, 8])

       self.game_state.attempt_upgrade.called_with([8, 7])

    def test_stall_with_interceptors(self):
        with patch("algo_strategy.AlgoStrategy.filter_blocked_locations") as filter_blocked_locations_mocked:
            game_map = Mock()
            self.algo_strategy.friendly_edges = [[0, 0], [0, 1], [1, 0], [1, 1]]
            filter_blocked_locations_mocked.side_effect = [[0, 1], [1, 0]]
            self.game_state.get_resource.side_effect = [4, 3, 2, 1, 0]
            self.game_state.type_cost.side_effect = [{1: 1}, {1: 1}, {1: 1}, {1: 1}, {1: 1}]
            self.game_state.game_map = game_map

            self.algo_strategy.stall_with_interceptors(self.game_state)

            self.assertEqual(filter_blocked_locations_mocked.call_count, 1)
            filter_blocked_locations_mocked.assert_called_with([[0, 0], [0, 1], [1, 0], [1, 1]], self.game_state)
            self.assertEqual(self.game_state.get_resource.call_count, 5)
            self.game_state.get_resource.assert_has_calls = [call(1), call(1), call(1), call(1), call(1)]
            self.assertEqual(self.game_state.type_cost.call_count, 5)
            self.game_state.type_cost.assert_has_calls = [call("INTERCEPTOR"), call("INTERCEPTOR"), call("INTERCEPTOR"), call("INTERCEPTOR"), call("INTERCEPTOR")]
            self.assertEqual(self.game_state.attempt_spawn.call_count, 4)

    def test_stall_with_interceptors_no_deploy_locations(self):
        with patch("algo_strategy.AlgoStrategy.filter_blocked_locations") as filter_blocked_locations_mocked:
            game_map = Mock()
            self.algo_strategy.friendly_edges = [[0, 0], [0, 1], [1, 0], [1, 1]]
            filter_blocked_locations_mocked.return_value = []
            self.game_state.get_resource.return_value = 4
            self.game_state.type_cost.return_value = {1: 1}
            self.game_state.game_map = game_map

            self.algo_strategy.stall_with_interceptors(self.game_state)

            self.assertEqual(filter_blocked_locations_mocked.call_count, 1)
            filter_blocked_locations_mocked.assert_called_with([[0, 0], [0, 1], [1, 0], [1, 1]], self.game_state)
            self.assertEqual(self.game_state.get_resource.call_count, 1)
            self.game_state.get_resource.assert_has_calls = [call(1)]
            self.assertEqual(self.game_state.type_cost.call_count, 1)
            self.game_state.type_cost.assert_has_calls = [call("INTERCEPTOR")]
            self.assertEqual(self.game_state.attempt_spawn.call_count, 0)

    def test_support_tactic_can_afford_four_supports(self):
        with patch("algo_strategy.AlgoStrategy.least_damage_spawn_location") as least_damage_spawn_location_mocked:
            with patch("algo_strategy.AlgoStrategy.get_support_coverage") as get_support_coverage_mocked:
                with patch("algo_strategy.AlgoStrategy.equip_spawn_group") as equip_spawn_group_mocked:
                    self.game_state.number_affordable.return_value = 4
                    least_damage_spawn_location_mocked.return_value = [17, 3]

                    self.assertEqual(self.algo_strategy.support_tactic(self.game_state), [17, 3])

                    self.game_state.number_affordable.assert_called_with("SUPPORT")
                    self.assertEqual(get_support_coverage_mocked.call_count, 0)
                    least_damage_spawn_location_mocked.assert_called_with(self.game_state, self.algo_strategy.spawn_locations)
                    self.assertEqual(equip_spawn_group_mocked.call_count, 1)
                    equip_spawn_group_mocked.assert_called_with(3, self.game_state)

    def test_support_tactic_can_not_afford_four_supports_and_fully_equipped_spawn_group(self):
        with patch("algo_strategy.AlgoStrategy.least_damage_spawn_location") as least_damage_spawn_location_mocked:
            with patch("algo_strategy.AlgoStrategy.get_support_coverage") as get_support_coverage_mocked:
                with patch("algo_strategy.AlgoStrategy.equip_spawn_group") as equip_spawn_group_mocked:
                    self.game_state.number_affordable.return_value = 3
                    get_support_coverage_mocked.return_value = {
                        0: 0,
                        1: 1,
                        2: 0,
                        3: 0,
                        4: 0
                    }
                    least_damage_spawn_location_mocked.return_value = [9, 4]

                    self.assertEqual(self.algo_strategy.support_tactic(self.game_state), [9, 4])

                    self.game_state.number_affordable.assert_called_with("SUPPORT")
                    self.assertEqual(get_support_coverage_mocked.call_count, 1)
                    least_damage_spawn_location_mocked.assert_called_with(self.game_state, [[9, 4], [10, 3]])
                    self.assertEqual(equip_spawn_group_mocked.call_count, 1)
                    equip_spawn_group_mocked.assert_called_with(1, self.game_state)

    def test_support_tactic_can_not_afford_four_supports_and_partially_and_fully_equipped_spawn_groups(self):
        with patch("algo_strategy.AlgoStrategy.least_damage_spawn_location") as least_damage_spawn_location_mocked:
            with patch("algo_strategy.AlgoStrategy.get_support_coverage") as get_support_coverage_mocked:
                with patch("algo_strategy.AlgoStrategy.equip_spawn_group") as equip_spawn_group_mocked:
                    self.game_state.number_affordable.return_value = 1
                    get_support_coverage_mocked.return_value = {
                        0: 0.5,
                        1: 0.5,
                        2: 0.75,
                        3: 0.75,
                        4: 1
                    }
                    least_damage_spawn_location_mocked.return_value = [12, 1]

                    self.assertEqual(self.algo_strategy.support_tactic(self.game_state), [12, 1])

                    self.game_state.number_affordable.assert_called_with("SUPPORT")
                    self.assertEqual(get_support_coverage_mocked.call_count, 1)
                    least_damage_spawn_location_mocked.assert_called_with(self.game_state, [[12, 1], [13, 0], [14, 0], [15, 1], [17, 3], [18, 4], [20, 6], [21, 7]])
                    self.assertEqual(equip_spawn_group_mocked.call_count, 1)
                    equip_spawn_group_mocked.assert_called_with(2, self.game_state)

    def test_support_tactic_can_not_afford_four_supports_and_no_partially_nor_fully_equipped_spawn_groups(self):
        with patch("algo_strategy.AlgoStrategy.least_damage_spawn_location") as least_damage_spawn_location_mocked:
            with patch("algo_strategy.AlgoStrategy.get_support_coverage") as get_support_coverage_mocked:
                with patch("algo_strategy.AlgoStrategy.equip_spawn_group") as equip_spawn_group_mocked:
                    self.game_state.number_affordable.return_value = 3
                    get_support_coverage_mocked.return_value = {
                        0: 0,
                        1: 0,
                        2: 0,
                        3: 0,
                        4: 0
                    }

                    self.assertEqual(self.algo_strategy.support_tactic(self.game_state), None)

                    self.game_state.number_affordable.assert_called_with("SUPPORT")
                    self.assertEqual(get_support_coverage_mocked.call_count, 1)
                    self.assertEqual(least_damage_spawn_location_mocked.call_count, 0)
                    self.assertEqual(equip_spawn_group_mocked.call_count, 0)

    def test_get_locations_for_spawn_groups(self):
        self.assertEqual(self.algo_strategy.get_locations_for_spawn_groups([0]), [[6, 7], [7, 6]])

    def test_equip_spawn_group_non_upgrade_case(self):
        self.algo_strategy.equip_spawn_group(1, self.game_state)
        self.assertEqual(self.game_state.attempt_spawn.call_count, 4)
        self.game_state.attempt_spawn.assert_has_calls([call("SUPPORT", [8, 5]), call("SUPPORT", [9, 6]), call("SUPPORT", [11, 2]), call("SUPPORT", [12, 3])], any_order=False)
        self.assertEqual(self.game_state.attempt_upgrade.call_count, 0)

    def test_equip_spawn_group_upgrade_case(self):
        self.algo_strategy.equip_spawn_group(0, self.game_state)
        self.assertEqual(self.game_state.attempt_spawn.call_count, 3)
        self.game_state.attempt_spawn.assert_has_calls([call("SUPPORT", [5, 8]), call("SUPPORT", [8, 5]), call("SUPPORT", [9, 6])], any_order=False)
        self.assertEqual(self.game_state.attempt_upgrade.call_count, 1)
        self.game_state.attempt_upgrade.assert_called_with([5, 8])

    def test_get_highest_coverage_spawn_groups(self):
            with patch("algo_strategy.AlgoStrategy.get_support_coverage") as get_support_coverage_mocked:
                get_support_coverage_mocked.return_value = {
                    0: 0,
                    1: 0.75,
                    2: 0.5,
                    3: 0.75,
                    4: 0.25
                }

                self.assertEqual(self.algo_strategy.get_highest_coverage_spawn_groups(self.game_state), [1, 3])

    def test_get_support_coverage_no_supports(self):
        contains_stationary_unit_side_effects = []
        for i in range(0, 18):
            contains_stationary_unit_side_effects.append(False)
        self.game_state.contains_stationary_unit.side_effect = contains_stationary_unit_side_effects

        result = self.algo_strategy.get_support_coverage(self.game_state)

        for value in result.values():
            self.assertEqual(value, 0)
        self.assertEqual(self.game_state.contains_stationary_unit.call_count, 18)

    def test_get_support_coverage_fully_covered_spawn_group(self):
        contains_stationary_unit_side_effects = []
        for i in range(0, 18):
            if i >= 3 and i <= 6:
                contains_stationary_unit_side_effects.append(True)
            else:
                contains_stationary_unit_side_effects.append(False)
        self.game_state.contains_stationary_unit.side_effect = contains_stationary_unit_side_effects

        result = self.algo_strategy.get_support_coverage(self.game_state)

        for spawn_group in result:
            if spawn_group != 1:
                self.assertEqual(result[spawn_group], 0)
            else:
                self.assertEqual(result[spawn_group], 1)
        self.assertEqual(self.game_state.contains_stationary_unit.call_count, 18)

    def test_get_support_coverage_fully_and_partially_spawn_groups(self):
        self.game_state.number_affordable.return_value = 1
        self.game_state.contains_stationary_unit.side_effect = [True, False, False, False, False, True, True, True, True, True, False, True, False, True, True, True, True, True]
        expected_result = {
            0: 0.5,
            1: 0.5,
            2: 0.75,
            3: 0.75,
            4: 1
        }

        result = self.algo_strategy.get_support_coverage(self.game_state)

        self.assertEqual(result, expected_result)
        self.assertEqual(self.game_state.contains_stationary_unit.call_count, 18)

    def test_get_spawn_group_of_location_non_spawn_location(self):
        self.assertEqual(self.algo_strategy.get_spawn_group_of_location([100, 100]), None)

    def test_get_spawn_group_of_location(self):
        self.assertEqual(self.algo_strategy.get_spawn_group_of_location(self.algo_strategy.spawn_locations[0]), 0)

    def test_least_damage_spawn_location_no_possible_spawn_locations(self):
        find_path_return_values = []
        for i in range(0, len(self.algo_strategy.spawn_locations)):
            find_path_return_values.append(None)
        self.game_state.find_path_to_edge.side_effect = find_path_return_values

        # if there are no possible spawn locations at all, the method should just return an empty array
        self.assertEqual(self.algo_strategy.least_damage_spawn_location(self.game_state, self.algo_strategy.spawn_locations), [])
        self.assertEqual(self.game_state.find_path_to_edge.call_count, len(self.algo_strategy.spawn_locations))

    def test_least_damage_spawn_location(self):
        with patch("algo_strategy.gamelib.GameUnit") as GameUnit_mocked:
            find_path_return_values = []
            get_attackers_calls = []
            for i in range(0, len(self.algo_strategy.spawn_locations)):
                if not i == 0:
                    find_path_return_values.append([[i, i]])
                    get_attackers_calls.append(call([i, i], 0))
                else:
                    find_path_return_values.append(None)
            self.game_state.find_path_to_edge.side_effect = find_path_return_values

            get_attackers_return_values = []
            attackers = []
            for i in range(0, len(self.algo_strategy.spawn_locations) - 1):
                attackers.append("TURRET")
                get_attackers_return_values.append(attackers)
            self.game_state.get_attackers.side_effect = get_attackers_return_values

            GameUnit_objects = []
            for i in range(0, len(self.algo_strategy.spawn_locations) - 1):
                game_unit = Mock()
                game_unit.damage_i = 1
                GameUnit_objects.append(game_unit)
            GameUnit_mocked.side_effect = GameUnit_objects

            self.assertEqual(self.algo_strategy.least_damage_spawn_location(self.game_state, self.algo_strategy.spawn_locations), [7, 6])

            self.assertEqual(self.game_state.find_path_to_edge.call_count, len(self.algo_strategy.spawn_locations))
            find_path_calls = []
            for i in range (0, len(self.algo_strategy.spawn_locations)):
                find_path_calls.append(call(self.algo_strategy.spawn_locations[i]))
            self.game_state.find_path_to_edge.assert_has_calls(find_path_calls, any_order=False)

            self.assertEqual(self.game_state.get_attackers.call_count, len(self.algo_strategy.spawn_locations) - 1)
            self.game_state.get_attackers.assert_has_calls(get_attackers_calls, any_order=False)
            self.assertEqual(GameUnit_mocked.call_count, len(self.algo_strategy.spawn_locations) - 1)

    def test_detect_enemy_unit_no_units(self):
        self.game_state.contains_stationary_unit.side_effect = [True, False]
        unit_0 = Mock()
        unit_0.player_index = 0
        unit_1 = Mock()
        unit_1.player_index = 1
        self.game_state.game_map = {"location_0": [unit_0], "location_1": [unit_1]}

        self.assertEqual(self.algo_strategy.detect_enemy_unit(self.game_state), 0)

    def test_detect_enemy_unit_one_unit(self):
        self.game_state.contains_stationary_unit.return_value = True
        unit_0 = Mock()
        unit_0.player_index = 1
        self.game_state.game_map = {"location_0": [unit_0]}

        self.assertEqual(self.algo_strategy.detect_enemy_unit(self.game_state), 1)

    def test_detect_enemy_unit_multiple_units(self):
        self.game_state.contains_stationary_unit.return_value = True
        unit_0 = Mock()
        unit_0.player_index = 1
        unit_1 = Mock()
        unit_1.player_index = 0
        unit_2 = Mock()
        unit_2.player_index = 1
        self.game_state.game_map = {"location_0": [unit_0, unit_1, unit_2]}

        self.assertEqual(self.algo_strategy.detect_enemy_unit(self.game_state), 2)

    def test_detect_enemy_unit_multiple_locations(self):
        self.game_state.contains_stationary_unit.side_effect = [True, True, True]
        unit_0 = Mock()
        unit_0.player_index = 1
        unit_1 = Mock()
        unit_1.player_index = 0
        unit_2 = Mock()
        unit_2.player_index = 1
        unit_3 = Mock()
        unit_3.player_index = 1
        unit_4 = Mock()
        unit_4.player_index = 0
        self.game_state.game_map = {"location_0": [unit_0, unit_1], "location_1": [unit_2, unit_3], "location_2": [unit_4]}

        self.assertEqual(self.algo_strategy.detect_enemy_unit(self.game_state), 3)

    def test_filter_blocked_locations(self):
        locations = [[0, 0], [0, 1], [0, 2]]
        self.game_state.contains_stationary_unit.side_effect = [True, False, True]

        self.assertEqual(self.algo_strategy.filter_blocked_locations(locations, self.game_state), [[0, 1]])


if __name__ == "__main__":
    unittest.main()
