import gamelib
import random
import math
import warnings
from sys import maxsize
import json


class AlgoStrategy(gamelib.AlgoCore):

    def __init__(self):
        super().__init__()
        seed = random.randrange(maxsize)
        random.seed(seed)
        gamelib.debug_write('Random seed: {}'.format(seed))
        self.scored_last_turn = False
        self.demolisher_assault_mode = False
        self.turn_of_assault_mode_activation = -1
        self.turns_since_last_assault = 0
        self.friendly_edges = None
        # edge locations that (turrets) can freely be built on (because it does not interfere with any tactic)
        self.non_reserved_edge_locations = [[0, 13], [1, 12], [2, 11], [3, 10], [4, 9], [23, 9], [24, 10], [25, 11], [26, 12], [27, 13]]
        self.spawn_locations = [[6, 7], [7, 6], [9, 4], [10, 3], [12, 1], [13, 0], [14, 0], [15, 1], [17, 3], [18, 4], [20, 6], [21, 7]]
        self.support_locations = [[5, 8], [8, 5], [9, 6], [11, 2], [12, 3], [15, 3], [16, 2], [18, 6], [19, 5], [22, 8]]
        self.spawn_groups_to_spawn_location_indices = {
            0: [0, 1],
            1: [2, 3],
            2: [4, 5, 6, 7],
            3: [8, 9],
            4: [10, 11]
        }
        self.spawn_groups_to_support_location_indices = {
            0: [0, 1, 2],
            1: [1, 2, 3, 4],
            2: [3, 4, 5, 6],
            3: [5, 6, 7, 8],
            4: [7, 8, 9]
        }
        self.spawn_groups_to_blockage_triggering_location_combinations = {
            0: [[[6, 9], [7, 8], [8, 7]]],
            1: [[[10, 5], [11, 4]]],
            2: [[[13, 2], [14, 1]], [[14, 2], [13, 1]], [[13, 1], [14, 1]], [[13, 2], [14, 2]]],
            3: [[[17, 5], [16, 4]]],
            4: [[[21, 9], [20, 8], [19, 7]]]
        }

    # Read in config and perform any initial setup here
    def on_game_start(self, config):
        gamelib.debug_write('Configuring the algo ...')
        self.config = config
        global WALL, SUPPORT, TURRET, SCOUT, DEMOLISHER, INTERCEPTOR, MP, SP
        WALL = config["unitInformation"][0]["shorthand"]
        SUPPORT = config["unitInformation"][1]["shorthand"]
        TURRET = config["unitInformation"][2]["shorthand"]
        SCOUT = config["unitInformation"][3]["shorthand"]
        DEMOLISHER = config["unitInformation"][4]["shorthand"]
        INTERCEPTOR = config["unitInformation"][5]["shorthand"]
        MP = 1
        SP = 0
        # This is a good place to do initial setup
        self.scored_on_locations = []

    """
    This method is called every turn with the game state wrapper as an argument.
    The wrapper stores the state of the arena and has methods for querying its state and allocating the current resources as planned
    """
    def on_turn(self, turn_state):
        game_state = gamelib.GameState(self.config, turn_state)
        gamelib.debug_write('Performing turn {} of your custom algo strategy'.format(game_state.turn_number))
        # game_state.suppress_warnings(True)

        if self.friendly_edges == None:
            self.friendly_edges = game_state.game_map.get_edge_locations(game_state.game_map.BOTTOM_LEFT) + game_state.game_map.get_edge_locations(game_state.game_map.BOTTOM_RIGHT)
        self.execute_strategy(game_state)
        self.scored_last_turn = False
        game_state.submit_turn()

    # This method can be called hundreds of times per turn and could slow the algo down so avoid putting slow code here
    def on_action_frame(self, turn_string):
        state = json.loads(turn_string)
        events = state["events"]
        breaches = events["breach"]
        for breach in breaches:
            location = breach[0]
            unit_owner_self = True if breach[4] == 1 else False
            # 1 is associated with us, 2 is associated with our enemy (StarterKit code uses 0, 1 as player_index instead)
            if not unit_owner_self:
                gamelib.debug_write("Got scored on at: {}".format(location))
                self.scored_on_locations.append(location)
                gamelib.debug_write("All locations: {}".format(self.scored_on_locations))
            else:
                self.scored_last_turn = True

    def execute_strategy(self, game_state):
        self.build_static_defense(game_state)
        self.build_reactive_defense(game_state)

        if game_state.turn_number < 5:
            # Save up mobile points for the initial assault (not incrementing the turns since last assault counter while preparing for the initial assault)
            return
        else:
            self.turns_since_last_assault += 1
            # Initial assault
            if game_state.turn_number == 5:
                scout_spawning_location = self.support_tactic(game_state)
                # Conduct the initial assault even if we can't afford to fully equip a spawn location
                if scout_spawning_location == None:
                    scout_spawning_location = self.least_damage_spawn_location(game_state, self.spawn_locations)
                    spawn_group = self.get_spawn_group_of_location(scout_spawning_location)
                    # Even if we can't afford to fully equip a spawn location, the program tries to equips it as best as we can afford
                    self.equip_spawn_group(spawn_group, game_state)
                game_state.attempt_spawn(SCOUT, scout_spawning_location, 1000)
                self.turns_since_last_assault = 0
            elif self.scored_last_turn == True:
                scout_spawning_location = self.support_tactic(game_state)
                if scout_spawning_location != None:
                    game_state.attempt_spawn(SCOUT, scout_spawning_location, 1000)
                    self.turns_since_last_assault = 0
            elif self.demolisher_assault_mode == False:
                self.demolisher_assault_mode = True
                self.turn_of_assault_mode_activation = game_state.turn_number
            elif self.demolisher_assault_mode == True and game_state.turn_number - self.turn_of_assault_mode_activation > 1:
                demolisher_spawning_location = self.support_tactic(game_state)
                if demolisher_spawning_location != None:
                    self.demolisher_assault_mode = False
                    game_state.attempt_spawn(DEMOLISHER, demolisher_spawning_location, 1000)
                    self.turns_since_last_assault = 0

        if self.turns_since_last_assault >= 4:
            highest_support_coverage_spawn_groups = self.get_highest_coverage_spawn_groups(game_state)
            spawn_locations = self.get_locations_for_spawn_groups(highest_support_coverage_spawn_groups)
            best_spawn_location = self.least_damage_spawn_location(game_state, spawn_locations)
            spawn_group = self.get_spawn_group_of_location(best_spawn_location)
            self.equip_spawn_group(spawn_group, game_state)
            game_state.attempt_spawn(DEMOLISHER, best_spawn_location, 1000)
            self.demolisher_assault_mode = False
            self.turns_since_last_assault = 0

    def build_static_defense(self, game_state):
        turret_locations = [[0, 13], [4, 12], [23, 12], [27, 13], [8, 11], [19, 11], [13, 11], [14, 11]]
        game_state.attempt_spawn(TURRET, turret_locations)
        wall_locations = [[4, 13], [23, 13], [8, 12], [19 , 12], [13, 12], [14, 12]]
        game_state.attempt_spawn(WALL, wall_locations)
        game_state.attempt_upgrade(wall_locations)
        game_state.attempt_upgrade(turret_locations)

    def build_reactive_defense(self, game_state):
        for location in self.scored_on_locations:
            if location in self.non_reserved_edge_locations:
                game_state.attempt_spawn(TURRET, location)
                trouble_locations = [[0, 13], [27, 13]]
                if location not in trouble_locations:
                    wall_built_amount = game_state.attempt_spawn(WALL, [location[0], location[1]+1])
                    if wall_built_amount > 0:
                        game_state.attempt_upgrade([location[0], location[1]+1])
            else:
                self.improve_defense_near_spawn_location(game_state, location)

    """
    This method has to be used for adding new turrets near a spawn location (for building near or on non reserved edge locations, there is no need to use this).
    It makes sure that no location that is reserved for the spawning of mobile units or supports is built on.
    It also makes sure that no blockage of any spawn group can occur
    """
    def improve_defense_near_spawn_location(self, game_state, location):
        build_location = [location[0], location[1]+2]
        built_new_turret = False
        # No need to check if the default location is on our own side because of the non reserved edge locations
        if build_location not in self.support_locations:
            built_new_turret = self.spawn_turret_if_no_blockage_of_spawn_group(game_state, build_location)
            if not built_new_turret:
                self.upgrade_turret_of_blockage_triggering_location_combinations(game_state, build_location)
        else:
            left_edge_build_location = [location[0]+2, location[1]]
            right_edge_build_location = [location[0]-2, location[1]]
            if game_state.game_map.in_arena_bounds(left_edge_build_location):
                build_location = left_edge_build_location
                built_new_turret = self.spawn_turret_if_no_blockage_of_spawn_group(game_state, left_edge_build_location)
                if not built_new_turret:
                    self.upgrade_turret_of_blockage_triggering_location_combinations(game_state, left_edge_build_location)
            else:
                build_location = right_edge_build_location
                built_new_turret = self.spawn_turret_if_no_blockage_of_spawn_group(game_state, right_edge_build_location)
                if not built_new_turret:
                    self.upgrade_turret_of_blockage_triggering_location_combinations(game_state, right_edge_build_location)
        if built_new_turret:
            narrowing_locations = [[5, 10], [22, 10]]
            if build_location not in narrowing_locations:
                if build_location == [13, 1] or build_location == [14, 1]:
                    wall_built_amount = game_state.attempt_spawn(WALL, [build_location[0], build_location [1]+2])
                    if wall_built_amount > 0:
                        game_state.attempt_upgrade([build_location[0], build_location [1]+2])
                else:
                    wall_built_amount = game_state.attempt_spawn(WALL, [build_location[0], build_location[1]+1])
                    if wall_built_amount > 0:
                        game_state.attempt_upgrade([build_location[0], build_location[1]+1])

    def spawn_turret_if_no_blockage_of_spawn_group(self, game_state, location):
        for spawn_group in self.spawn_groups_to_blockage_triggering_location_combinations:
            blockage_combinations = self.spawn_groups_to_blockage_triggering_location_combinations[spawn_group]
            for blockage_combination in blockage_combinations:
                if location in blockage_combination:
                    built_counter = 0
                    for partial_blockage_location in blockage_combination:
                        if game_state.contains_stationary_unit(partial_blockage_location):
                            built_counter += 1
                    if len(blockage_combination) <= built_counter + 1:
                        return False
        turret_built_amount = game_state.attempt_spawn(TURRET, location)
        # if no turret was built (because of not enough money/already existing stationary unit on that location)
        if turret_built_amount == 0:
            return False
        return True

    def upgrade_turret_of_blockage_triggering_location_combinations(self, game_state, location):
        for spawn_group in self.spawn_groups_to_blockage_triggering_location_combinations:
            blockage_combinations = self.spawn_groups_to_blockage_triggering_location_combinations[spawn_group]
            for blockage_combination in blockage_combinations:
                if location in blockage_combination:
                    for turret_location in blockage_combination:
                        if game_state.contains_stationary_unit(turret_location):
                            game_state.attempt_upgrade(turret_location)
                            return

    # Sends out interceptors at random locations to defend our base from enemy mobile units
    def stall_with_interceptors(self, game_state):
        deploy_locations = self.filter_blocked_locations(self.friendly_edges, game_state)
        while game_state.get_resource(MP) >= game_state.type_cost(INTERCEPTOR)[MP] and len(deploy_locations) > 0:
            deploy_index = random.randint(0, len(deploy_locations) - 1)
            deploy_location = deploy_locations[deploy_index]
            game_state.attempt_spawn(INTERCEPTOR, deploy_location)
            # No need to remove the location we built on since multiple mobile units can occupy the same space

    def support_tactic(self, game_state):
        number_affordable = game_state.number_affordable(SUPPORT)
        if number_affordable < 4:
            affordable_spawn_groups = []
            spawn_groups_to_support_coverage = self.get_support_coverage(game_state)
            for spawn_group in spawn_groups_to_support_coverage:
                if spawn_groups_to_support_coverage[spawn_group] + number_affordable/4 >= 1:
                    affordable_spawn_groups.append(spawn_group)
        else:
            affordable_spawn_groups = [0, 1, 2, 3, 4]

        if affordable_spawn_groups == []:
            return None

        affordable_spawn_locations = self.get_locations_for_spawn_groups(affordable_spawn_groups)
        best_spawn_location = self.least_damage_spawn_location(game_state, affordable_spawn_locations)
        spawn_group = self.get_spawn_group_of_location(best_spawn_location)
        self.equip_spawn_group(spawn_group, game_state)
        return best_spawn_location

    def get_locations_for_spawn_groups(self, spawn_groups):
        spawn_locations =[]
        for spawn_group in spawn_groups:
            for spawn_location_index in self.spawn_groups_to_spawn_location_indices[spawn_group]:
                spawn_locations.append(self.spawn_locations[spawn_location_index])
        return spawn_locations

    def equip_spawn_group(self, spawn_group, game_state):
        support_location_indices = self.spawn_groups_to_support_location_indices[spawn_group]
        for index in support_location_indices:
            game_state.attempt_spawn(SUPPORT, self.support_locations[index])
            if index in [0, 9]:
                game_state.attempt_upgrade(self.support_locations[index])

    def get_highest_coverage_spawn_groups(self, game_state):
        spawn_groups_with_highest_coverage = []
        spawn_groups_to_support_coverage = self.get_support_coverage(game_state)
        max_coverage_quotient = max(spawn_groups_to_support_coverage.values())
        for spawn_group in spawn_groups_to_support_coverage:
            coverage_quotient = spawn_groups_to_support_coverage[spawn_group]
            if coverage_quotient == max_coverage_quotient:
                spawn_groups_with_highest_coverage.append(spawn_group)

        return spawn_groups_with_highest_coverage

    def get_support_coverage(self, game_state):
        spawn_groups_to_support_coverage = {}
        for spawn_group in self.spawn_groups_to_support_location_indices:
            support_location_index_list = self.spawn_groups_to_support_location_indices[spawn_group]
            needed_support_number = 4
            existing_support_counter = 0
            for support_location_index in support_location_index_list:
                if game_state.contains_stationary_unit(self.support_locations[support_location_index]):
                    existing_support_counter += 1
                    # If there is an upgraded support, increment existing_support_counter by one, since it costs as much to upgrade an existing support as it costs to spawn a new one
                    if support_location_index == 0:
                        existing_support_counter += 1
                    if support_location_index == 9:
                        existing_support_counter += 1

            spawn_groups_to_support_coverage[spawn_group] = existing_support_counter/needed_support_number

        return spawn_groups_to_support_coverage

    def get_spawn_group_of_location(self, location):
        try:
            spawn_location_index = self.spawn_locations.index(location)
        except ValueError:
            return None
        spawn_location_indices_per_spawn_group = list(self.spawn_groups_to_spawn_location_indices.values())
        index = 0
        for spawn_location_index_list in spawn_location_indices_per_spawn_group:
            if spawn_location_index in spawn_location_index_list:
                break
            index += 1
        return list(self.spawn_groups_to_spawn_location_indices.keys())[index]

    def least_damage_spawn_location(self, game_state, location_options):
        damages = []
        valid_locations = []
        for location in location_options:
            path = game_state.find_path_to_edge(location)
            if not path == None:
                valid_locations.append(location)
                damage = 0
                for path_location in path:
                    damage += len(game_state.get_attackers(path_location, 0)) * gamelib.GameUnit(TURRET, game_state.config).damage_i
                damages.append(damage)
        
        try:
            return valid_locations[damages.index(min(damages))]
        # If there is no valid location
        except ValueError:
            return []

    def detect_enemy_unit(self, game_state, unit_type=None, valid_x = None, valid_y = None):
        total_units = 0
        for location in game_state.game_map:
            if game_state.contains_stationary_unit(location):
                for unit in game_state.game_map[location]:
                    if unit.player_index == 1 and (unit_type is None or unit.unit_type == unit_type) and (valid_x is None or location[0] in valid_x) and (valid_y is None or location[1] in valid_y):
                        total_units += 1
        return total_units
        
    def filter_blocked_locations(self, locations, game_state):
        filtered = []
        for location in locations:
            if not game_state.contains_stationary_unit(location):
                filtered.append(location)
        return filtered


if __name__ == "__main__":
    algo = AlgoStrategy()
    algo.start()
