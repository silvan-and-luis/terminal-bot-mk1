# Terminal Bot Mk1

Simple bot for the game "Terminal" by Correlation One, written in Python.  
The code changes of Merge Requests need to be evaluated by uploading the code of the given branch to Terminal and letting it run there for a while.  
This project is regarded as being finished.  

[Terminal homepage](https://terminal.c1games.com/)  
[Tool for converting graphically selected tiles to coordinates](https://www.kevinbai.design/terminal-map-maker/)  

## Result (as of August 2021)
![result](result.png)

## Possible error when uploading an algo to Terminal

When uploading an algo to Terminal, the following error can occur:  
"/bin/sh: 1: ./run.sh: not found"  

It is caused by Unix line breaks being replaced by DOS line breaks.  
The error can be resolved by using a converter to replace the line endings in "run.sh".  
["dos2unix"](https://sourceforge.net/projects/dos2unix/) can be used for the conversion.  
